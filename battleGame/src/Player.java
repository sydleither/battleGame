import java.util.ArrayList;

public class Player {
    public String getPlayer(){
        ArrayList<String> playerChoices = new ArrayList<>();
        playerChoices.add("bananacat.png");
        playerChoices.add("cyclonecat.png");
        playerChoices.add("loopscat.png");
        playerChoices.add("twolegcat.png");

        return playerChoices.get((int)(Math.random()*4));
    }
}
