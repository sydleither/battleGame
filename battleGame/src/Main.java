import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.net.URL;

//Sydney Leither 2018
public class Main extends Application{
    MediaPlayer mp;
    MediaPlayer hitNoise;
    Pane pane = new Pane();
    Player player = new Player();
    Enemy enemy;

    Image backgroundV = new Image(getClass().getResourceAsStream("resources/background.jpg"));
    ImageView background = new ImageView(backgroundV);
    Image playerImageV = new Image(getClass().getResourceAsStream("resources/"+player.getPlayer()));
    ImageView playerImage = new ImageView(playerImageV);
    Image enemyImageV;
    ImageView enemyImage;
    Image laserImageV = new Image(getClass().getResourceAsStream("resources/laser.png"));
    ImageView laserImage = new ImageView(laserImageV);

    int score = 0;
    int updateTime = 28;
    Text scoreText = new Text("Score: "+score);
    Text gameOver = new Text("Game Over");

    int playerSpeed = 15;

    boolean laserShot = false;
    int laserDirection = 0;
    int laserSpeed = 15;
    double laserX = 0;
    double laserY = 0;

    int size = 100;
    double x = 450;
    double y = 450;
    int xV = 0;
    int yV = 0;

    int timer = 0;

    public void start(Stage primaryStage){
        //background music
        URL resource = getClass().getResource("resources/music.mp3");
        Media media = new Media(resource.toString());
        mp = new MediaPlayer(media);

        //hit noise
        URL resource2 = getClass().getResource("resources/oof.mp3");
        Media media2 = new Media(resource2.toString());
        hitNoise = new MediaPlayer(media2);

        playerImage.setPreserveRatio(true);

        enemy = new Enemy();
        enemyImageV = new Image(getClass().getResourceAsStream("resources/"+enemy.getEnemy()));
        enemyImage = new ImageView(enemyImageV);
        xV = enemy.getxV();
        yV = enemy.getyV();

        pane.getChildren().add(background);
        pane.getChildren().add(enemyImage);
        pane.getChildren().add(playerImage);
        pane.getChildren().add(scoreText);
        pane.getChildren().add(laserImage);

        background.setX(0);
        background.setY(0);

        playerImage.setX(0);
        playerImage.setY(0);

        scoreText.setFont(Font.font("Comic Sans",20));
        scoreText.setFill(Color.RED);
        scoreText.setX(10);
        scoreText.setY(20);

        gameOver.setFont(Font.font("Comic Sans",50));
        gameOver.setFill(Color.RED);
        gameOver.setX(400);
        gameOver.setY(500);

        Duration dI = new Duration(updateTime);
        KeyFrame f = new KeyFrame(dI, e -> movement());
        Timeline tl = new Timeline(f);
        tl.setCycleCount(Animation.INDEFINITE);
        tl.play();
        Scene scene = new Scene(pane, 1000, 1000);
        scene.setOnKeyPressed(e -> keyboardManage(e));

        mp.setCycleCount(MediaPlayer.INDEFINITE);
        mp.play();

        primaryStage.setTitle("Battle Game");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void keyboardManage(KeyEvent e){
        //moving the player and finding what way they're facing
        double x = playerImage.getX();
        double y = playerImage.getY();

        if(e.getCode() == KeyCode.RIGHT){
            laserDirection = 0;
            x += playerSpeed;
            playerImage.setX(x);
        }

        else if(e.getCode() == KeyCode.LEFT){
            laserDirection = 1;
            x -= playerSpeed;
            playerImage.setX(x);
        }

        else if(e.getCode() == KeyCode.UP){
            laserDirection = 2;
            y -= playerSpeed;
            playerImage.setY(y);
        }

        else if(e.getCode() == KeyCode.DOWN){
            laserDirection = 3;
            y += playerSpeed;
            playerImage.setY(y);
        }

        if(e.getCode() == KeyCode.SPACE){
                laserShot = true;
                laserX = playerImage.getX();
                laserY = playerImage.getY();
        }
    }

    public void movement(){
        //timer to prevent collisions from happening too fast
        timer++;

        //random movement of enemy on screen
        if(x<=0 || x+size>=1000){
            xV *= -1;
        }
        if(y<=0 || y+size>=1000){
            yV *= -1;
        }

        x += xV;
        y += yV;

        enemyImage.setX(x);
        enemyImage.setY(y);

        //laser movement
       if(laserShot){
            if(laserDirection == 0){
                laserX += laserSpeed;
                laserImage.setX(laserX);
                laserImage.setY(laserY);
            }

           if(laserDirection == 1){
               laserX -= laserSpeed;
               laserImage.setX(laserX);
               laserImage.setY(laserY);
           }

           if(laserDirection == 2){
               laserY -= laserSpeed;
               laserImage.setX(laserX);
               laserImage.setY(laserY);
           }

           if(laserDirection == 3){
               laserY += laserSpeed;
               laserImage.setX(laserX);
               laserImage.setY(laserY);
           }
      }

      //checking to see if enemy is still alive and creating a new one
      if(intersects(laserImage, enemyImage, 10, 100) && timer > 100){
          hitNoise.play();

          score++;
          scoreText.setText("Score: "+(score));

          pane.getChildren().remove(enemyImage);

          enemy = new Enemy();
          enemyImageV = new Image(getClass().getResourceAsStream("resources/"+enemy.getEnemy()));
          enemyImage = new ImageView(enemyImageV);
          pane.getChildren().add(enemyImage);

          laserImage.setX(0);
          laserImage.setY(0);

          timer = 0;
      }

      //allow replay of hit noise
      if(timer > 50){
          hitNoise.stop();
      }

      //checking to see if player is still alive
      if(intersects(playerImage, enemyImage, 100, 100) && timer > 100){
          pane.getChildren().remove(enemyImage);
          pane.getChildren().remove(playerImage);
          pane.getChildren().remove(scoreText);
          pane.getChildren().remove(laserImage);

          //stopping the game
          try {
              pane.getChildren().add(gameOver);
          }
          catch(IllegalArgumentException e){ }
      }
    }

    public boolean intersects(ImageView image1, ImageView image2, int size1, int size2){
        Rectangle2D rectangle1 = new Rectangle2D(image1.getX(),image1.getY(), size1, size1);
        Rectangle2D rectangle2 = new Rectangle2D(image2.getX(),image2.getY(), size2, size2);
        return rectangle1.intersects(rectangle2);
    }

    public static void main(String[] args){
        launch(args);
    }
}