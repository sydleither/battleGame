import java.util.ArrayList;

public class Enemy {
    private int xV, yV;

    public Enemy(){
        xV = (int)(Math.random()*30-15);
        yV = (int)(Math.random()*30-15);
    }

    public String getEnemy(){
        ArrayList<String> enemyChoices = new ArrayList<>();
        enemyChoices.add("capedog.png");
        enemyChoices.add("taildog.png");
        enemyChoices.add("sockdog.png");
        enemyChoices.add("readydog.png");

        return enemyChoices.get((int)(Math.random()*4));
    }

    public int getxV(){
        return this.xV;
    }

    public int getyV(){
        return this.yV;
    }
}
